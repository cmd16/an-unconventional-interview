"An Unconventional Interview" by Catherine DeJager.
The story headline is "A curious test of creativity".
The story genre is "Comedy".
The story description is "A strange sort of job interview where you have free reign to interact with a variety of objects in the room, all while a scientist is reacting and evaluating your personality traits. Explore, try all the wild ideas your heart desires, and above all have fun!".
The story creation year is 2021.

Release along with the introductory booklet, an introductory postcard, and an interpreter.

Use serial comma.

Include Basic Screen Effects by Emily Short. Include Menus by Emily Short. Include Basic Help Menu by Emily Short.
Include Inanimate Listeners by Emily Short.

Table of Basic Help Options (continued)
title			subtable		description				toggle
"Actions for this game"			Table of Relevant Actions		"The primary actions to get interesting results"				--
"Contacting the Author"			--		"If you are having trouble or otherwise wish to share your feedback, leave a comment on the itch.io page."				--

Table of Relevant Actions
title		subtable	description
"answering      'math, 42'"		--	"Answer one of the math questions."
"asking         'ask scientist about score'"		--	"Ask someone about something."
"attacking      'hit table'"		--	"Attack something/someone."
"burning        'burn scientist'"		--	"Burn something/someone. No firestarters in this game, but you do get some reactions."
"climbing       'climb table'"		--	"Get on top of something."
"cutting        'cut salad'"		--	"Chop or slice something/someone."
"drinking       'drink lake'"		--	"Drink something."
"dropping       'drop salad'"		--	"Let go of something you are carrying."
"eating         'eat salad'"		--	"Eat something."
"examining      'examine table'"		--	"Look at something to get more detail about it."
"inserting      'put salad pieces in salad'"		--	"Put something inside something else"
"inventory      'inventory'"		--	"See what you are carrying."
"kissing        'kiss scientist'"		--	"Kiss something/someone."
"knocking       'knock on table'"		--	"Knock on something."
"listening to   'listen to scientist'"		--	"Listen to something."
"pulling        'pull salad'"		--	"Pull something."
"pushing        'push salad'"		--	"Push something."
"putting it on  'put knife on table'"		--	"Put something on something else."
"stabbing       'stab scientist'"		--	"Stab something."
"taking         'take knife'"		--	"Pick something up."
"throwing it at 'throw knife at scientist'"		--	"Throw something at something/someone else."
"waving hands   'wave'"		--	"Wave in greeting."
"wearing        'wear salad'"		--	"Wear something (usually for clothing, but you can try wearing other things)."

[
lab
expression
investment
variety
lake
personality
society
math
worker
salad
]

A thing can be viewed or unviewed. A thing is usually unviewed.
A thing can be chopped. A thing is seldom chopped.
A person can be kissable. A person is seldom kissable.
A thing can be climbable or unclimbable. A thing is seldom climbable.
A thing can be tainted. An edible thing is seldom tainted.
A thing can be stained. A thing is seldom stained.
A thing can be damaged or undamaged. A thing is seldom damaged.
A thing can be stuck or unstuck. A thing is seldom stuck.
A thing can be wet or dry. A thing is usually dry.

Definition: A thing is pristine if it is not damaged and it is not stained.

A personality score is a kind of value. 0 points specifies a personality score.

Boldness is a number that varies. Max boldness is always 31.0.
Weirdness is a number that varies. Max weirdness is always 50.0.
Rudeness is a number that varies. Max rudeness is always 59.0.
Politeness is a number that varies. Max politeness is always 10.0.
Practicality is a number that varies. Max practicality is always 24.0.

bold behaviors is a list of stored actions that varies.
weird behaviors is a list of stored actions that varies.
rude behaviors is a list of stored actions that varies.
polite behaviors is a list of stored actions that varies.
practical behaviors is a list of stored actions that varies.

Climbing something unclimbable is bold behavior. Eating something inedible is weird behavior. Eating an edible thing that is unviewed is bold behavior. Eating an edible thing that is viewed is practical behavior. Attacking a person is rude behavior. Pulling a person is rude behavior. Pushing a person is rude behavior. Waving hands is polite behavior. Examining something is practical behavior.

Before bold behavior (this is the bold rule):
	if the current action is not listed in bold behaviors:
[		say "bold";]
		increment boldness;
		add the current action to bold behaviors;
	continue the action.

Before weird behavior (this is the weird rule):
	if the current action is not listed in weird behaviors:
[		say "weird";]
		increment weirdness;
		add the current action to weird behaviors;
	continue the action.

Before rude behavior (this is the rude rule):
	if the current action is not listed in rude behaviors:
[		say "rude";]
		increment rudeness;
		add the current action to rude behaviors;
	continue the action.

Before polite behavior (this is the polite rule):
	if the current action is not listed in polite behaviors:
[		say "polite";]
		increment politeness;
		add the current action to polite behaviors;
	continue the action.

Before practical behavior (this is the practical rule):	
	if the current action is not listed in practical behaviors:
[		say "practical";]
		increment practicality;
		add the current action to practical behaviors;
	continue the action.

After examining something:
	now the noun is viewed;
	continue the action.

Taking inventory is acting confused. Looking is acting confused. Examining a viewed thing is acting confused.
After acting confused for the sixth turn: 
    say "(If you are feeling lost, try typing help for suggestions. In particular, look at [bold type]Actions for this game[roman type].)".

The description of the player is "Just another test subject[if the player is damaged], with a cool-looking knife scar on your face that sets you apart from the crowd![else if the player is not chopped], with a bland face.[end if][if the player is wearing the salad hat] You're wearing a cool salad hat, which makes you look both cute and healthy, like your crush at the gym![end if][if the player is tainted] You've got an awkward haircut (a sloppy, jagged line making one side of your hair about half its original length) that significantly detracts from your coolness.[end if][if the player is chopped] Your upper lip has a small gash that looks way more painful and impressive than it actually is.[end if][if the player is wet] You are also dripping wet.[end if]".

Instead of cutting something when the knife is not carried:
	say "Your arm moves forward in a smooth cutting motion, but then you realize you don't have anything to cut with. Awkward!".

Burning someone is rude behavior.
Instead of burning something:
	say "You briefly consider arson, but then realize you're missing the equipment to start a fire. Good news for the room, and for your criminal record, but bad news for your pyromaniac tendencies.".

Instead of burning something wet:
	say "You can hardly burn something wet, now can you? Didn't they teach you anything in fire school?".

Instead of buying something:
	say "You try to hand some of your money to [the scientist] and ask if you can have [the noun]. [They] laughs and inform you that no purchases are necessary: you are welcome to take anything you can carry, though you will need to return it when you have completed all of the tests.";

Instead of climbing something unclimbable:
	say "You attempt to clamber over [the noun], and end up falling on your face. Not your most graceful moment, but hey, at least you tried! [The scientist] bursts into laughter.";

Instead of climbing something climbable (called the surface):
	now the player is on the surface.

Instead of drinking something:
	say "As tasty as [the noun] might be, you'd need to liquidate [them
	] first.[if the noun is a person] Liquidating people is generally frowned upon.[end if] Though maybe if [they] had liquid [italic type]on[roman type] [them], that would be something you could drink."

Drinking something wet is weird behavior.
Instead of drinking something wet:
	say "You lick the water off [the noun]. [The scientist] chuckles.";
	try drying the noun.

Eating someone is rude behavior.
Instead of eating someone:
	say "Cannabalism is most definitely illegal, and you're really not in a mood to go to jail."

Kissing something that is not a person is weird behavior.
Instead of kissing something:
	say "You blow [the noun] a kiss. No response, of course. [The scientist] takes pity on you and blows you a kiss.".

Instead of throwing something at the player:
	say "You attempt the awkward hand maneuvers to throw [the noun] at yourself. Alas, your arms are simply too short. [The scientist] valiantly supresses [their] laughter for all of two seconds, and then that laughter is there in full force.".

The standard report eating rule is not listed in any rulebook.
The block kissing rule is not listed in the check kissing rulebook.
Understand "kiss [something]" as kissing.
The describe room stood up into rule is not listed in the report getting off rulebook.
The describe room emerged into rule is not listed in the report exiting rulebook.
Rule for printing room description details: stop.

The lab is a room. The description is "[if unvisited]You step into the room for your job interview. Instead of the standard setup with two chairs, you observe a distinct lack of chairs. You see [a scientist] in a lab coat. [They] notices you looking at [them] and strides forward to shake your hand.[paragraph break][quotation mark]Welcome! You're here for the interview, I take it? You may have noticed we do things a little differently here. This is the personality test, a sort of practical exam, if you will. Don't worry, there are no right or wrong answers, just a wide variety of possibilities. This is about learning what kind of worker you are so we can see how you best fit in the wonderful society that is our company! While it may feel like a waste of time, please see this as an investment in your future. Any questions? No? Great! We begin now. Have fun, and show us what you're capable of![quotation mark][paragraph break][end if]You find yourself in a lab. Calling it that would be a bit generous, though, as there's not much here. Perhaps whoever designed this room wanted to keep things simple?". The printed name is "Lab".

The scientist is a woman in the lab. The description is "[A scientist], wearing a[if the coat is pristine] pristine[end if][if the coat is wet] wet[end if][if the coat is stained]tomato-stained[end if] white lab coat[if the coat is damaged] with a single slash mark[end if]. [They] is carrying a clipboard. Tucked behind [their] ear is a pencil which [they] frequently pulls out to make notes. [They] notices your attention and gives you an appraising look. You're pretty sure a scientist is supposed to be inexpressive so as not to bias the results, but this scientist seems to have a hard time doing that. At least that means you have more opportunities for entertainment![if the scientist is wet] [The scientist] is dripping wet, as is everything [they] is carrying.[end if]".

The scientist is wearing a coat. The printed name of the coat is "white lab coat". The description of the coat is "A nice, white, professional lab coat. [if the coat is stained]It's decidedly less white now that it's been covered in tomatoes.[end if][if the coat is damaged]It's got a gash through it from the knife, which makes it definitely less effective but possibly more cool.[end if][if the coat is not stained and the coat is not damaged]It would be a shame if something were to happen to it.[end if]".

The scientist is carrying a clipboard. 
Instead of examining the clipboard:
	say "A very professional-looking clipboard.[if the clipboard is wet] You can see that a soggy page has been flipped over to reach a slighlty less soggy (and thus able to be written on) page.[end if]";
	say "Between the abysmal handwriting and [the scientist]'s constant shifting of the clipboard to keep you from getting a good view, you can't tell what is written on it.". 
[	if a random chance of 1 in 2 succeeds:
		say "You can read.";
	else:
		say "Between the abysmal handwriting and [the scientist]'s constant shifting of the clipboard to keep you from getting a good view, you can't tell what is written on it.". ]

The scientist is carrying a pencil. The description of the pencil is "Just a typical graphite writing instrument. It has an inscription on the side that says [quotation mark]It's not science unless you write it down![quotation mark][if the pencil is damaged] Upon further inspection, you notice another inscription: [quotation mark]If you are reading this, there is a pencil destroyer in the area. Beware![quotation mark][end if][if the pencil is tainted] Upon further inspection, you notice another inscription: [quotation mark]If you are reading this, there is a pencil thief in the area. Beware![quotation mark][end if][if the pencil is wet] The pencil is a bit wet, but it appears to still be functional.[end if]". The printed name of the pencil is "pencil[if the pencil is damaged or the pencil is tainted] (replacement)[end if]".

Every turn when the scientist is visible:
	say "[The scientist] [one of]scribbles something on [their] clipboard[or]writes something on [their] clipboard[or]makes a note[or]notes something on [their] clipboard[or]jots something down on [their] clipboard[or]doodles in the margins of [their] clipboard... no, wait, that's writing not doodles[or]writes hastily on [their] clipboard[or]writes deliberately on [their] clipboard[or]writes decisively on the clipboard[or]makes another judgement, at least you assume that's what the writing on [their] clipboard is[or]takes some notes on [their] clipboard[or]records another observation on [their] clipboard[or]confidently writes something down on [their] clipboard[or]slowly notes something on [their] clipboard[or]moves the pencil swiftly along the clipboard paper to make another note[or]moves the pencil hesitantly along the clipboard paper to make another note[at random].[paragraph break]";
	if the remainder after dividing the turn count by 15 is 0:
		say "'In case you've forgotten,' [they] says, 'when you are ready to end the test, just [bold type]ask about results[roman type]. Please don't feel rushed, though, there's still plenty to explore!'".

[Instead of doing something to something when the player is on a supporter (called the surface):
	if the noun is not the surface and the noun is not carried by the pl
	try getting off the surface;
	continue the action.]

Instead of asking the scientist about "score":
	set pronouns from the scientist;
	say "'It would be unprofessional to reveal results while the test is still ongoing,' [the scientist] tells you. It seems if you want to know your score, you'll need to get more creative than that.";
	say "'When you are ready to conclude, ask me about [quotation mark]results[quotation mark] and I'll be happy to share.'".

Instead of asking the scientist about "personality":
	try asking the scientist about "score".

Instead of asking the scientist about "test":
	try asking the scientist about "score".

Instead of asking the scientist about "results":
	say "'Are you ready for your results?' (yes/no)[line break]";
	if the player consents:
		let bold percent be boldness / max boldness;
		let weird percent be weirdness / max weirdness;
		let rude percent be rudeness / max rudeness;
		let polite percent be politeness / max politeness;
		let practical percent be practicality / max practicality;
		say "[paragraph break]Your boldness is      [boldness] points (out of [max boldness] possible): [bold percent * 100 to 2 decimal places]%.";
		say "Your weirdness is    [weirdness] points (out of [max weirdness] possible): [weird percent * 100 to 2 decimal places]%.";
		say "Your rudeness is      [rudeness] points (out of [max rudeness] possible): [rude percent * 100 to 2 decimal places]%.";
		say "Your politeness is    [politeness] points (out of [max politeness] possible): [polite percent * 100 to 2 decimal places]%.";
		say "Your practicality is   [practicality] points (out of [max practicality] possible): [practical percent * 100 to 2 decimal places]%.";
		say "[paragraph break][quotation mark]And now, for the job recommendations:[paragraph break]";
		let offer be false;
		if bold percent is greater than 0.25:
			say "'You know how to take charge and take risks. You would be a great fit for our investment department! The current personnel are all rather cautious, so we've been keeping our eyes out for someone to push them into trying some more ideas that could have bigger payoffs.'[paragraph break]";
			now offer is true;
		if weird percent is greater than 0.25:
			say "'Your mind works in mysterious ways. You would be a fantastic fit for our innovation department! That kind of unconventional, off-the-wall thinking is exactly what will set us apart from our competitors.'[paragraph break]";
			now offer is true;
		if rude percent is greater than 0.20:
			say "'You excel at breaking social norms and just generally displaying rudeness. You would be an excellent fit for our humor department! Clearly, you're not afraid use your quick wit!'[paragraph break]";
			now offer is true;
		if polite percent is greater than 0.25:
			say "'You've demonstrated you really do care for all the little social niceties. You would do wonderful in our PR department! To be frank, most of us are incredibly socially awkward, so we could really benefit from someone polite to show our customers how much we care.'[paragraph break]";
			now offer is true;
		if practical percent is greater than 0.25:
			say "'You've got a sensible head on your shoulders, and that's a trait that will serve you well in life. You would be a perfect fit for our accounting department! Your thorough, analytical mind means you must be excellent at math.'[paragraph break]";
			now offer is true;
		if offer is false:
			say "'Unfortunately, you didn't really stand out compared to the rest of the many applicants we've seen. Don't worry, though! We believe anyone can improve, so you're welcome to take this test again at any time. Be sure to be thorough and try lots of different things so we can get a good sense what you are like.'";
			end the story finally saying "The End. (You may want to check out the help menu to see what actions are supported in this game.)";
		else:
			say "'So, what do you think? Do any of those offers look good to you?' (yes/no)[line break]";
			if the player consents:
				say "[paragraph break]'Fantastic! We're so glad to have you as part of the team.'";
			else:
				say "[paragraph break]'I'm sorry to hear that! We do hope you'll reconsider. If you're unsatisfied with your results, you're welcome to take the test again at another time, and other opportunities may open up!'";
			end the story finally;
[		say "Your bold actions were [bold behaviors].";
		say "Your weird actions were [weird behaviors].";
		say "Your rude actions were [rude behaviors].";
		say "Your polite actions were [polite behaviors].";
		say "Your practical actions were [practical behaviors].";]
	else:
		say "'No problem, take all the time you need.'";

Instead of asking the scientist about "result":
	try asking the scientist about "results".

Rule for printing a parser error when the latest parser error is can't see any such thing error and the player's command matches the text "result":
	try asking the scientist about "results".

Instead of attacking the scientist:
	say "You move in for an attack on [the scientist], but [they] blocks your moves effortlessly, [their] stoic gaze never wavering. You make a mental note to ask [them] for martial arts training, since [they're] clearly very capable. Not now, though. Maybe after the tests are over.".

Instead of attacking the coat:
	try attacking the scientist.

Attacking the clipboard is rude behavior.
Instead of attacking the clipboard:
	say "You crumple up a piece of paper on [the scientist]'s clipboard! [They] scowls and tosses the paper at you. [They] then begins hastily writing down the data from memory.".

Attacking the pencil is rude behavior.
Instead of attacking the pencil:
	say "You whack the pencil out of [the scientist]'s hands. You can see the frown building on [their] face.[paragraph break]";
	move the pencil to the lab;
	the scientist replaces the pencil in one turn from now.

Burning the scientist is rude behavior.
Instead of burning the scientist:
	say "You take a moment to contemplate your sickest burn, given what you know of the scientist.[paragraph break]";
	say "'My 3-year old sister is more professional than you!'";
	say "[The scientist] bursts into tears!".

Climbing the scientist is rude behavior.
Instead of climbing the scientist:
	say "You try to hop onto [the scientist] for a piggy-back. You miss, and land painfully on you butt. [They] turns to look at you and immediately gives up on the impossibilty of not laughing. Eventually, the laughter subsides and [they] returns to [their] usual blank expression.".

Cutting the scientist is rude behavior.
Instead of cutting the scientist:
	say "You make a threatening throat-slitting gesture, which [the scientist] mirrors.".

Instead of cutting the scientist when the knife is carried:
	say "You hold the knife up to [the scientist]'s throat. [The scientist] holds still for a moment, treating you to a condescending stare, then [they] knocks the knife out of your hand.";
	move the knife to the lab.

Cutting the coat is rude behavior.
Instead of cutting the coat:
	say "You fantasize about slashing up that beautiful white lab coat, but unfortunately you cannot carry out your ideas as you don't have anything on your person suitable for cutting with.".

Instead of cutting the coat when the knife is carried:
	say "You make a single slice into the arm of the lab coat, being careful not to reach through to the skin. [The scientist] permits this one slice, then [they] pulls the knife out of your hands and puts it on the table. [They] narrows [their] eyes as [they] awaits your next move.";
	move the knife to the table;
	now the coat is damaged.

Instead of cutting the coat when the knife is carried and the coat is damaged:
	say "No more lab coat chopping allowed! [The scientist] yanks the knife out of your hands and sets it firmly back down on the table.";
	move the knife to the table.

Instead of drinking the scientist when the scientist is wet:
	say "You attempt to lick the water off [the scientist], but it seems [they] is not in the mood to be licked. [They] wrings out [their] coat on you in retaliation! That smug grin on [their] face is unforgettable.";
	try drying the scientist;
	try soaking the player.

After eating an edible thing that is unviewed (called food):
	set pronouns from the scientist;
	say "Eating food without even looking at it first? [The scientist] cackles. You hope you correctly interpreted that as an innocuous chuckle and not a manaical laugh. [They] wouldn't poison test subjects, right? That would never pass a review board! [italic type]Did[roman type] these experiments pass a review board? Maybe you should have read the job listing more carefully.";
	set pronouns from food;
	continue the action.

Asking the scientist about "kiss" is polite behavior.
Instead of asking the scientist about "kiss":
	say "You ask [the scientist] if you can kiss [them]. [quotation mark]Oh![quotation mark] [They] considers for a moment.";
	say "[Their] expression softens. 'I'll allow that as part of the experiment, but only on the cheek. I don't want to make things too awkward.'";
	now the scientist is kissable;

Rule for printing a parser error when the latest parser error is can't see any such thing error and the player's command matches the text "kiss":
	try asking the scientist about "kiss".

Instead of kissing the scientist:
	say "You walk over to [the scientist] and [if the scientist is not kissable]lean in to [end if]kiss [them].";
	if the scientist is not kissable:
		say "'You know, it's very impolite to kiss someone without asking first,' [the scientist] says, fixing you with a stern gaze. After an uncomfortable silence, [their] eyes return to [their] clipboard.";
	else:
		say "[The scientist] blushes and quickly returns to [their] clipboard.";
		say "Brief, but it was nice while it lasted.".

Instead of kissing the clipboard:
	say "You lean forward and press an obnoxious kiss mark on the front page of the clipboard. [The scientist] rapidly scribbles it out and aggressively flips to the next page.".

Instead of kissing the coat:
	say "You kiss [the coat]. It tastes like fabric[if the coat is tainted], salad, [end if] and strange chemicals that are hopefully not toxic. [The scientist] takes a large step back.".

Instead of kissing the pencil:
	say "You attempt to kiss [the pencil], but you miss and kiss [the scientist]'s hand instead. [They] gives a mock curtsy and an exaggerated smile, batting [their] eyelashes.".

Listening to the scientist is polite behavior. Listening to the scientist is practical behavior.
Instead of listening to the scientist:
	say "You take a few moments to savor the sweet music of pencil on clipboard as [the scientist] continues taking notes. You hear a few [quotation mark]hmm[quotation mark]s and sighs as [they] considers the situation. [They] smiles when [they] notices your careful observation, then returns to [their] notetaking. Eventually, the noise subsides. It seems when you aren't doing much of anything, there's not much to note. [They] looks up at you expectantly.".

Instead of listening to the clipboard:
	try listening to the scientist.

Instead of listening to the pencil:
	try listening to the scientist.

Pulling the scientist is rude behavior.
Instead of pulling the scientist:
	say "You reach forward to grab [the scientist] and pull [them], but [they] grabs your hands long before they reach [them]. [They] sure has quick reflexes! [They] gives a weary look that indicates you're not the first to try this and you certainly won't be the last.".

Pushing the scientist is rude behavior.
Instead of pushing the scientist:
	say "You walk up to [the scientist] to push [them]. You can see [their] brow furrowed in thought as [they] decides whether or not to allow this.";
	say "[They] makes no move to stop you. You attempt to push [them], but [they] holds [their] ground with a confident stare. You wonder if there was some sort of physical fitness test required for this job.".

Instead of waving something:
	try showing the noun to the scientist.

Report showing something to the scientist:
	say "You show [the noun] to [the scientist]. [quotation mark]Yes, I can see that![quotation mark] [the scientist] says with a quizzical smile."

Instead of waving hands:
	say "You wave to [the scientist]. [They] waves back. 'We've already been acquainted, but hello again!'"

The table is a supporter in the lab. The description is "A good, sturdy-looking wooden table.[if the table is damaged] It's a little worse for the wear given your recently inflicted mutilation.[end if][if the knife is stuck and the knife is on the table] The knife is sticking out of it, handle up.[end if] It looks about the right size for dinner with friends, if you don't have many friends." The printed name of the table is "[if the table is damaged]scratched [end if]table". The table is climbable. The table is fixed in place.

Attacking the table is weird behavior.
Instead of attacking the table:
	say "You give the table a good whack. That hurts! [The scientist] smirks at you.".

Cutting the table is rude behavior.
Instead of cutting the table:
	if the knife is carried:
		say "You slice into the table with the knife. This knife is definitely not made for chopping wood, but nevertheless you manage to cut a sizable groove into the surface. That's gonna take some work to recover. [The scientist] snarls at you. Apparently, property damage is a pretty serious concern.";
	else:
		say "You decide to use the best cutting instrument available to you: your disturbingly sharp fingernails. You scratch a long line. [The scientist] scowls at you.";
	now the table is damaged.

Climbing the table is weird behavior.
Report climbing the table:
	say "You climb on top of the table. You feel much taller now!";
	say "[The scientist] looks up to you... correction, [they] looks up [italic type]at[roman type] you. With that blank expression [they] has (presumably in an attempt to appear neutral), you can't tell what [they] thinks of you.".

Instead of eating the table:
	say "You lean down and bite a small chunk out of the table. It has a distinctly wooden taste. You hope you won't end up with splinters from this. [The scientist] gasps.";
	now the table is damaged.

Instead of kissing the table:
	say "You run your lips over the nice, long wood. No, that definitely doesn't have any other connotations, what are you talking about? [The scientist] winks at you.".

Knocking is an action applying to one thing. Understand "knock [something]" and "knock on [something]" as knocking.
Carry out knocking:
	say "You knock on [the noun]. No response.".

Instead of knocking a person:
	say "You go to give [the noun] a light punch on the shoulder.";
	try attacking the noun.

Knocking the table is practical behavior.
Instead of knocking the table:
	say "You knock on the wooden table. Good luck! [The scientist] gives you a thumbs-up."

Throwing the table at something is bold behavior.
Before throwing the table at something:
	say "You confidently stride up to the table and grab it to throw at [the second noun], but then you realize you don't have the strength to pick up this table by yourself, let alone throw it. [The scientist] arches an incredulous eyebrow.";
	stop the action.

Wearing the table is weird behavior.
Before wearing the table:
	now the player carries the table.

Instead of wearing the table:
	say "You crawl underneath the table and then position youself such that you can pretend you are a turtle and the table is your shell. [The scientist] laughs so hard [they] cries![paragraph break]";
	say "Eventually, you decide your days of table-wearing are over, at least for now. You go back to standing in the room like a boring person.";
	move the table to the lab.

[knock]

The salad is on the table. The printed name is "[if the salad is tainted]tainted [end if][if the salad is wet]soggy [end if][if the salad is chopped]chopped [end if]salad". The description is "A typical salad[if the salad is chopped], chopped finely. It is[end if] packed full of a variety of vegetables, particularly the large, leafy ones. Non-leafy vegetables in the salad include tomatoes and carrots. You feel healthier just looking at it. No dressing on this one, though: it seems whoever made this salad deemed the improved taste not worth the extra calories.[if the salad is tainted] It looks a bit dirty, probably due to the salad pieces you added from the floor.[end if][if the salad is wet] It's a bit mushy from the water, but probably still edible.[end if]". The salad is edible.

There are a few salad pieces. The description is "The pieces of salad you have so unceremoniously dumped on the floor. You wonder whether being on the floor makes the salad more or less healthy.[if the salad pieces are wet] They look all limp and soggy from the water, which does not make them any more appetizing.[end if]". The salad pieces are edible.

The salad hat is wearable. The description is "An awkward but slightly charming assortment of salad pieces that can be placed in someone's hair. Does wearing this mean you have a head of lettuce? At any rate, you've achieved [quotation mark]salad dressing![quotation mark][if the salad hat is wet] And it might as well be covered in salad dressing, because this hat is drenched![end if]". Understand "salad flowers" or "hair/head salad" or "salad hair/head" as salad hat. The salad hat is edible.

Does the player mean doing something to salad pieces when the salad is somewhere: it is unlikely.
Does the player mean doing something to salad hat when the salad is somewhere: it is unlikely.

Eating the salad is polite behavior.
Report eating the salad:
	if the salad is wet:
		say "Wet salad is not a great texture, but maybe the taste will be better.[paragraph break]";
	if the salad is tainted:
		say "Mostly good, but you can detect a trace of what you suspect to be carpet. Gross! This was probably a lot fresher before you put those floor pieces on it.";
	else:
		say "It tastes delicious! It's been a while since you've tasted food this fresh.[if the salad is chopped] The chopping really helped intensify the flavors, too. You feel very proud of yourself.[end if]";
	if the salad is viewed:
		say "You can't be sure since it was so quick, but you think you saw [the scientist]'s eyes light up and the corners of of [their] mouth turn up into a slight smile.";

Attacking the salad is rude behavior.
Instead of attacking the salad when the salad pieces are nowhere:
	say "You flail awkwardly at the salad, knocking a few pieces out of the bowl.";
	if the salad is wet:
		now the salad pieces are wet;
	else:
		now the salad pieces are dry;
	move the salad pieces to the lab;
	the salad pieces get dirty in two turns from now;
	say "[The scientist] looks around and sighs. You're not sure if this is her job to clean up, but you've definitely make more work for somebody.";

Instead of attacking the salad:
	say "You flail awkwardly at the salad, but since the bowl is already missing a few pieces, no more fall out.".

Instead of attacking the salad for the second time:
	say "Wow, you really have it out for this salad! You expect that's what [the scientist] would tell you if [they] wasn't concerned about biasing the test, though you do notice an eyebrow raise.";
	if the salad pieces are nowhere:
		if the salad is wet:
			now the salad pieces are wet;
		else:
			now the salad pieces are dry;
		move the salad pieces to the lab;
		now the salad pieces are edible;
		the salad pieces get dirty in two turns from now;
		say "Again, you knock a few pieces out of the bowl. [The scientist] rolls [their] eyes. It seems [they] has had enough of your nonsense.";
	else:
		say "Luckily for the salad, the bowl is empty enough that nothing is knocked out of it this time.".

Dropping the salad is rude behavior.
After dropping the salad:
	say "The salad clatters to the floor as your clumsy (or cruel?) fingers loosen their grip. [The scientist] clenches [their] fists.";
	if the salad pieces are nowhere:
		say "A few pieces go flying out due to your negligence.";
		now the salad pieces are edible;
		if the salad is wet:
			now the salad pieces are wet;
		else:
			now the salad pieces are dry;
		move the salad pieces to the lab;
		the salad pieces get dirty in two turns from now;
		say "[The scientist] looks around and sighs. You're not sure if this is her job to clean up, but you've definitely make more work for somebody.";
	continue the action.

Kissing the salad is polite behavior.
Instead of kissing the salad:
	say "You blow a kiss to the salad in appreciation for the chef's work. [The scientist] nods, beaming.".

Pulling the salad is rude behavior.
Instead of pulling the salad when the salad is on the table:
	say "You pull the salad to the edge of the table and carefully balance it on the edge. You feel pretty proud of yourself (and [the scientist] prepares to applaud), until it goes just a little bit too far and falls on the floor. [The scientist] withdraws [their] hands.";
	move the salad to the lab;
	if the salad pieces are nowhere:
		say "A few pieces topple out.";
		now the salad pieces are edible;
		if the salad is wet:
			now the salad pieces are wet;
		else:
			now the salad pieces are dry;
		move the salad pieces to the lab;
		the salad pieces get dirty in two turns from now;		
		say "[The scientist] looks around and sighs. You're not sure if this is her job to clean up, but you've definitely made more work for somebody.".

Pushing the salad is rude behavior.
Instead of pushing the salad when the salad is on the table:
	say "Like a capricious cat, you push the salad right off the table. It clatters to the ground. [The scientist] gives the same sort of smiling sigh one might give a mischevous pet.";
	move the salad to the lab;
	if the salad pieces are nowhere:
		say "A few pieces slip out over the edges of the bowl.";
		now the salad pieces are edible;
		if the salad is wet:
			now the salad pieces are wet;
		else:
			now the salad pieces are dry;
		move the salad pieces to the lab;
		the salad pieces get dirty in two turns from now;
		say "[The scientist] looks around and sighs. You're not sure if this is her job to clean up, but you've definitely made more work for somebody.".

Throwing the salad at something is rude behavior.
Instead of throwing the salad at the scientist:
	say "You throw the salad at [the scientist]. The tomatoes fly out and splatter [their] [coat]. The rest of the salad bits land on the floor, though miraculously there is still some salad in the bowl. [The scientist] looks at [their] coat, then back at you, then at the salad pieces on the floor. [They] gives a long sigh.";
	now the coat is stained;
	now the salad pieces are edible;
	if the salad is wet:
		now the salad pieces are wet;
	else:
		now the salad pieces are dry;
	move the salad pieces to the lab;
	the salad pieces get dirty in two turns from now.

Instead of throwing the salad at the coat:
	try throwing the salad at the scientist.

Instead of throwing the salad at the table:
	say "You violently hurl the salad at the table. A bit unnecessary given that you could have just set it down, but hey, [the scientist] did say this is your chance to be creative. [The scientist] mimics your throwing gesture and sticks [their] tongue out.";
	if the salad pieces are nowhere:
		say "A few pieces spill out in the action.";
		now the salad pieces are edible;
		if the salad is wet:
			now the salad pieces are wet;
		else:
			now the salad pieces are dry;
		move the salad pieces to the lab;
		the salad pieces get dirty in two turns from now;
		say "[The scientist] looks around and sighs. You're not sure if this is her job to clean up, but you've definitely made more work for somebody.".

At the time when the salad pieces get dirty:
	now the salad pieces are not edible.

Instead of throwing the salad at something:
	say "You consider this course of action and start moving as if to do so, but then you realize [the second noun] is so much smaller than the salad that this action would be meaningless. [The scientist] grimaces at your incompetence.".

Wearing the salad is weird behavior.
Instead of wearing the salad:
	say "You take a few pieces out of the salad and put them in your hair like flowers. It's... certainly a unique look! [The scientist] giggles.";
	if the salad is wet:
		now the salad hat is wet;
		try soaking the player;
	else:
		now the salad hat is dry;
	now the player is wearing the salad hat;
	the salad hat falls down in one turn from now.

Instead of wearing the salad for the second time:
	say "It's tempting, there's not really enough salad left in there to make another hat.[if the salad pieces are somewhere] Perhaps you could wear the salad pieces instead?";

Instead of dropping the salad hat:
	say "You let the pieces of salad fall out of your hair and onto the floor. [The scientist] frowns at this carelessness.";
	now the salad hat is nowhere;
	if the salad hat is wet:
		now the salad pieces are wet;
	else:
		now the salad pieces are dry;
	move the salad pieces to the lab.

At the time when the salad hat falls down:
	if the salad hat is not nowhere:
		say "Your movement jostles the salad loose from your hair. [The scientist] pouts a little, though you're not sure if that's due to the loss of the hat or the fact that there are now pieces of salad on the floor.";
		now the salad hat is nowhere;
		if the salad hat is wet:
			now the salad pieces are wet;
		else:
			now the salad pieces are dry;
		move the salad pieces to the lab.

Wearing the salad pieces is weird behavior.
Instead of wearing the salad pieces:
	if the salad hat has been worn:
		say "You reconstruct the salad hat. [The scientist] giggles again. It seems this joke never gets old!";
	else:
		say "You take the salad pieces and put them in your hair like flowers. They look better than they did on the floor! [The scientist] giggles.";
	now the salad pieces are nowhere;
	if the salad pieces are wet:
		now the salad hat is wet;
		try soaking the player;
	else:
		now the salad hat is dry;
	now the player is wearing the salad hat;
	the salad hat falls down in one turn from now.

Eating the salad hat is weird behavior.
Report eating the salad hat:
	say "It tastes like salad, but also hair. Not a great combination, to be honest.[if the salad hat is wet] And it's also wet and slimy, which makes the whole situation even worse.[end if] [The scientist] winces as if [they] is imagining how awful it must taste.".

Does the player mean doing something to salad pieces when the salad is somewhere: it is unlikely.
Does the player mean doing something to salad hat when the salad is somewhere: it is unlikely.
Does the player mean inserting something into something when the second noun is the salad pieces: it is very unlikely.

Before eating salad pieces when salad pieces are edible:
	say "Surely the five-second rule applies to bits of salad on the floor, right?";
	continue the action.

Eating salad pieces is weird behavior.
Report eating salad pieces:
	say "You take a bite[if the salad pieces are wet] of the little salad mush[end if]. [if the salad is nowhere]It tastes substantially worse than the salad did. That floor is really a corrupting influence![else]Not great, but maybe the salad that hasn't been tainted by the floor would be better.[end if]";
	say "[The scientist]'s eyes widen a little.";

Instead of eating salad pieces when the salad pieces are inedible:
	say "You consider eating the salad pieces, but you realize they've been sitting on the floor too long for that to be a good idea."

Taking salad pieces is polite behavior.
Report taking salad pieces:
	say "[The scientist] gives you an appreciative nod.";

Inserting salad pieces into the salad is rude behavior.
Instead of inserting salad pieces into the salad:
	say "Attempting to undo your mistake, you hastily shove the salad pieces into the salad. And now the whole salad is tainted! [The scientist] gags and looks away.";
	now the salad is tainted;
	now the salad pieces are nowhere.

Cutting the salad is practical behavior. Cutting the salad is polite behavior.
Instead of cutting the salad when the knife is carried:
	say "You pull out the knife and expertly slice through the salad.";
	now the salad is chopped;
	now the printed name of the salad is "chopped salad";
	increment politeness.

Putting the salad on the table is polite behavior.
Report putting the salad on the table:
	say "[The scientist] watches like a hawk, [their] gaze never wavering from you. [They] seems to relax when you place the salad back in its rightful place.";

The knife is on the table. The description is "A nice, big knife. Probably meant for slicing, chopping, or cutting... those are all about the same aren't they? It's a knife, what more did you expect?" Understand "blade" as knife.

Report dropping the knife when the player is not in the lake:
	say "You haphazardly slip the knife from your fingers. You hear a sharp intake of breath from [the scientist] as [they] watches with dread, but all is well. No injuries here. So much for learning a lesson!"

Instead of eating the knife:
	say "You bring the knife close to your lips and prepare for the unpleasant deed. All of [the scientist]'s muscles tense.[paragraph break]";
	say "But at the last minute you change your mind. Your face is too pretty to disfigure, and you'd rather not damage your mouth. Why eat a knife when you could eat something that's edible and [italic type]doesn't[roman type] cut up your tongue?";
	say "Noticing you've reconsidered, [the scientist] lets go of the tension at last."

Kissing the knife is bold behavior.
Instead of kissing the knife:
	say "You carefully plant a tender kiss on the knife, but despite all your caution you still end up slicing your lip.";
	now the player is chopped.

Putting the knife on the salad is weird behavior.
Instead of putting the knife on the salad:
	if the knife is not carried:
		try taking the knife;
		if the knife is not carried:
			stop the action;
	say "You carefully balance the knife on the salad, making this healthy meal much more deadly. Especially given that it (probably) wasn't deadly to begin with.";
	say "After staring at the situation for a while, you realize this is probably not a great place for a knife. You put the knife back on the table.";
	now the knife is on the table.

Pushing the knife is bold behavior.
Instead of pushing the knife when the knife is on the table:
	say "You give the knife a little nudge and it slides all the way off the table, dropping with a satisfying metal clang! [The scientist] eyes it cautiously.";
	move the knife to the lab.

Pulling the knife is bold behavior.
Instead of pulling the knife when the knife is on the table:
	say "You pull the knife by the handle, taking care so as not to slice your fingers. It teeters on the edge of the table. You and [the scientist] both keep your eyes locked on it, wondering whether or not it will fall.[paragraph break]";
	say "Eventually, you can't take it any longer and you decide to pull it all the way down to the floor. [The scientist] gives a small shrug.";
	move the knife to the lab.

Report taking the knife when the knife is stuck:
	say "Like a hero from arthurian legend, you pull the blade from [the noun]. [The scientist] gives you hearty applause and a wide grin.";
	now the knife is unstuck.

Throwing the knife at something is bold behavior. 

Throwing the knife at the scientist is rude behavior.
Instead of throwing the knife at the scientist:
	say "You hurl what is definitely not a throwing knife at [the scientist]. [They] dodges at the last possible moment and spread out [their] arms with a smile.";
	move the knife to the lab.

Instead of throwing the knife at the coat:
	try throwing the knife at the scientist.

Throwing the knife at the table is rude behavior.
Instead of throwing the knife at the table:
	say "You throw the knife at the table. It lands with a satisfying thud, and the tip becomes embedded in the center of the table. [The scientist] gives a golf clap.";
	now the table is damaged;
	now the knife is stuck;
	move the knife to the table.

Throwing the knife at the salad is weird behavior.
Instead of throwing the knife at the salad:
	say "You try to throw the knife at the salad, but the shot goes embarrasingly wide, and the knife falls to the floor. [The scientist] points at the knife and slowly  [their] head.";
	move the knife to the lab.

Throwing the knife at the salad pieces is weird behavior.
Instead of throwing the knife at the salad pieces:
	say "You attempt to throw the knife at the salad pieces, but the target is too small, so sadly the knife hits the floor rather than the intended target. [The scientist] shrugs and smirks in an 'I don't know what you expected to happen' sort of gesture.";
	move the knife to the lab.

Instead of throwing the knife at the salad hat:
	say "You try to throw the knife at the hat you are currently wearing. The result is an impromptu haircut, and it doesn't look nearly as good as the salad hat did. You can tell based on the way it feels and also on the fact that [the scientist] took a moment to retch.";
	now the player is tainted;
	move the knife to the lab.

Instead of throwing the knife at the pencil:
	say "You attempt to throw the knife at the pencil, but your aim is really not that precise. [The scientist] shakes [their] fist at your unskilled attempt.";
	if the scientist carries the pencil:
		try throwing the knife at the scientist.

Stabbing is an action applying to one thing. Understand "stab [something]" as stabbing.
Instead of stabbing when the knife is not carried:
	say "You make a stabbing gesture at [the noun], but without a suitable tool you are unable to carry out the stabbing. [The scientist] snorts in derision."

Instead of stabbing when the knife is carried:
	try cutting the noun.

Stabbing the scientist is bold behavior. Stabbing the scientist is rude behavior.
Instead of stabbing the scientist when the knife is carried:
	say "Dramatic music starts playing out of nowhere as you prepare to stab [the scientist]. You manage to poke a hole in [their] coat that quickly turns into a tear. [They] glares at you with a look that pierces harder than any knife ever could. Speaking of the knife, it's on the table. [They] takes it from you and slams it down on the table quite aggressively!";
	now the coat is damaged;
	move the knife to the table.

Instead of stabbing the coat:
	try stabbing the scientist.

Instead of stabbing the salad when the knife is carried:
	say "You attempt to stab the salad, but muscle memory kicks in and you end up chopping it instead.";
	try cutting the salad.

Cutting the pencil is rude behavior.
Instead of cutting the pencil when the knife is carried:
	say "You slice the pencil, snapping it in half. [The scientist] stares sadly at the waste of a perfectly good writing instrument.[paragraph break]";
	say "Once the mourning period is over, [they] pockets the broken pencil and produces a replacement pencil, identical to the old one.";
	now the pencil is damaged.

Stabbing the pencil is rude behavior.
Instead of stabbing the pencil when the knife is carried:
	say "You attempt to impale the pencil with the knife's sharp point. You are unsuccessful in this endeavor, but you do succeed in knocking the pencil out of [the scientist]'s hands. You can see the frown building on [their] face.[paragraph break]";
	move the pencil to the lab;
	the scientist replaces the pencil in one turn from now.

At the time when the scientist replaces the pencil:
	if the scientist does not have the pencil:
		say "[The scientist] furrows [their] brow, deep in thought.[paragraph break]";
		say "Eventually, it is decided. Rather than taking the effort to retreive the pencil, [they] will just get out one of [their] replacements.";
		say "Much to your confusion, this results in the original pencil disappearing. Perhaps replacements work differently here?";
		now the scientist has the pencil;
		now the pencil is tainted.

Giving the pencil to the scientist is polite behavior.
Instead of giving the pencil to the scientist:
	say "You give [the scientist] [their] pencil back! [Their] eyes light up in what feels to you like disproportionate excitement for the return of a simple writing instrument.";
	move the pencil to the scientist.

Cutting the clipboard is rude behavior.
Instead of cutting the clipboard when the knife is carried:
	say "You slash through a piece of paper on [the scientist]'s clipboard. [They] scowls and tosses the paper at you. [They] then begins hastily writing down the data from memory."

Putting the knife on the table is polite behavior. Putting the knife on the table is practical behavior.
Report putting the knife on the table:
	say "[The scientist] smiles as you remove [the knife] from its hazardous position and put it on the table."

Stabbing the table is bold behavior. Stabbing the table is rude behavior.
Instead of stabbing the table when the knife is carried:
	say "You sink the knife firmly and violently into the table. When you let go, the knife remains in the table, though you're not sure how securely it is stuck. [The scientist] groans.";
	now the knife is stuck;
	move the knife to the table.

Wearing the knife is bold behavior. Wearing the knife is weird behavior.
Instead of wearing the knife:
	say "You carefully balance the knife on your head. It's pretty cool, and [the scientist] gives an impressed [quotation mark]hmm[quotation mark][paragraph break]";
	say "Of course, all good things must come to an end. Sooner rather than later, in the case of this knife. It lands on the floor, giving you a cool scar on the way down.";
	now the player is damaged;
	move the knife to the lab.

The lake is a transparent container in the lab. The lake is enterable. The lake is fixed in place. The lake is wet. The description is "A large body of water that takes up much of the floor space. Technically, it should probably be classified as a pond, but for certain reasons that may not be known to you, this is most definitely called a [bold type]lake[roman type].".

Soaking is an action applying to one thing.
Carry out soaking:
	now the noun is wet;
	repeat with the item running through the list of things carried by the noun:
		now the item is wet;
	repeat with the item running through the list of things worn by the noun:
		now the item is wet.

Report soaking:
	let soaked list be the list of things carried by the noun;
	add the list of things worn by the noun to soaked list;
	say "[The noun] [are] now wet[if the number of entries in soaked list is greater than 0], and [soaked list with definite articles] [are] all wet, too[end if].".

Drying is an action applying to one thing.
Carry out drying:
	if the noun is in something wet:
		stop the action;
	else:
		now the noun is dry;
		repeat with the item running through the list of things carried by the noun:
			now the item is dry;
		repeat with the item running through the list of things worn by the noun:
			now the item is dry.

After taking something when the player is wet:
	try soaking the noun;
	continue the action.

After putting something on a supporter (called the surface):
	if the noun is wet:
		try soaking the second noun;
	else if the second noun is wet:
		try soaking the noun;
	continue the action.

After inserting something into a wet thing:
	try soaking the noun;
	continue the action.

Attacking the lake is weird behavior.
Instead of attacking the lake when the player is not in the lake:
	say "You swing forward to attack a body of water, but all that does is push your momentum forward so you fall into the lake.";
	try entering the lake.

Instead of attacking the lake when the player is in the lake:
	say "You flail about in the lake, splashing wildly like a little child. [The scientist] chuckles.".

Climbing the lake is weird behavior.
Instead of climbing the lake when the player is not in the lake:
	say "You attempt to climb on water. Water! Needless to say, you are unsuccessful. Instead, you fall into the lake.";
	try entering the lake.

Instead of climbing the lake when the player is in the lake:
	say "You tread water as if going up an imaginary staircase. [The scientist] shrugs, then stares off to the distance as if contemplating the many different kinds of staircases.".

Cutting the lake is weird behavior.
Instead of cutting the lake when the player has the knife:
	say "If this were a frozen lake, that might make sense. As it is, the knife slips from your hands and heads for the lake. [The scientist] sheds a few tears. Think of all the rust!";
	try dropping the knife.

Drinking the lake is weird behavior.
Instead of drinking the lake:
	say "You take a nice big gulp of the lake. So cool and refreshing! [The scientist] takes a nice big gulp of air and [their] expression is not cool or refreshing. You hope nothing too awful was in that lake...".

Instead of drinking the lake for the second time:
	say "If you're hoping to drain the lake, it's not going to work. You can only drink so much water. [The scientist] shakes [their] head.".

Instead of eating the lake:
	try drinking the lake.

Understand "jump in [something]" as entering.

After entering the lake:
	try soaking the player;
	continue the action.

Report entering the lake:
	say "[The scientist] sees you move to jump in the lake and tries to get out of the splash zone.";
	if a random chance of 2 in 3 succeeds:
		say "[They] was too slow! Now [they]'s soaking wet. [They] crosses [their] arms and pouts.";
		try soaking the scientist;
	else:
		say "[They] makes it out of the way just in time. [They] wipes the sweat from [their] brow.".

Understand "exit lake" as exiting. Understand "leave lake" as exiting. Understand "get out lake" as exiting. Understand "get out of lake" as exiting.

Report dropping something when the player is in the lake:
	say "You drop [the noun] into the lake with a splash! [The scientist]'s head goes down as [they] follows [the noun]'s trajectory, and then that movement turns into a facepalm in the smoothest transition you've ever seen.";

Inserting something into the lake is rude behavior.
Report inserting something into the lake:
	say "[The noun] sinks into the depths of the lake. Luckily for you, the lake is shallow and clear enough that you can still see [the noun]. Nevertheless, [the scientist] sobs for a short time.".

Report soaking the clipboard:
	say "[The scientist] sighs and with a smooth, practiced movement, flips over the waterlogged page to reveal a less waterlogged page. [They] resumes writing.".

Swimming is an action applying to nothing. Understand "swim" as swimming. Understand "swim lake" as swimming.
Swimming is practical behavior.
Instead of swimming:
	say "You need to be in a body of water first!".

Before swimming:
	if the player is not in the lake:
		try entering the lake.

Instead of swimming when the player is in the lake:
	say "You swim around in the lake, showing off your skills. [The scientist] quickly writes something on [their] clipboard and holds up a page with [bold type]10[roman type] written on it!".

Instead of kissing the lake:
	say "It is, without a doubt, the wettest kiss you've ever experienced.".

Knocking the lake is weird behavior.
Instead of knocking the lake:
	say "'Splash splash!' Is the response. Typical lake. It won't even bother waving to you!".

Listening to the lake is polite behavior. Listening to the lake is practical behavior.
Instead of listening to the lake:
	say "You take a moment to listen to the flowing water and appreciate its beauty. [The scientist] gives a small smile and nod at this brief tranquility.".

Pulling the lake is bold behavior.
Instead of pulling the lake:
	say "You attempt some sort of terraforming, but the lake resists your pull with all the force of an irresistable tide, and your pulling accomplishes nothing. [The scientist] rasises a skeptical eyebrow as if to ask 'what were you expecting?'".

Pushing the lake is bold behavior.
Instead of pushing the lake:
	say "You attempt some sort of terraforming, but the immovable object has met a stoppable force, and your pushing accomplishes nothing. [The scientist] rolls [their] eyes.".

Instead of putting something on the lake:
	say "Just like a person falling through a broken chair, [the noun] falls through a lake that definitely does not support its weight. [The scientist] facepalms so hard you wonder if that's given [them] a concussion.";
	try inserting the noun into the lake.

Instead of stabbing the lake when the player has the knife:
	try cutting the lake.

Instead of throwing something at the lake:
	say "You throw [the noun] at the lake. It's a rather large, easy target, so it's pretty hard to miss.";
	if the noun is not the lake:
		try inserting the noun into the lake.

Throwing the lake at something is rude behavior.
Before throwing the lake at something:
	now the player carries the lake.

Instead of throwing the lake at something:
	say "You throw some water from the lake at [the second noun]. [The scientist] shakes [their] head in disapproval.";
	if the second noun is not the lake:
		try soaking the second noun;
	move the lake to the lab.

Wearing the lake is weird behavior.
Before wearing the lake when the player is not in the lake:
	now the player carries the lake.

Instead of wearing the lake when the player is in the lake:
	say "Kind of hard to wear something you're already inside!".

Instead of wearing the lake when the player is not in the lake:
	say "Maybe if you jump into the lake, you can pretend it's a really big cloak you're wearing!";
	move the lake to the lab;
	try entering the lake.

The math is an addressable thing on the table. The printed name of the math is "math worksheet". Understand "worksheet" or "math worksheet" or "paper" as the math.

Instead of examining the math:
	say "The paper has a few simple arithmetic operations[if the math is chopped]still visible despite the slash marks[end if]:[paragraph break]";
	let indexes be a list of numbers;
	let labels be {"bold", "weird", "rude", "polite", "practical"};
	sort labels in random order;
	let operations be {"x", "/", "+", "-"};
	sort operations in random order;
	let answer be a number;
	repeat with label running through labels:
		if label is "bold":
			now answer is boldness;
		else if label is "weird":
			now answer is weirdness;
		else if label is "rude":
			now answer is rudeness;
		else if label is "polite":
			now answer is politeness;
		else if label is "practical":
			now answer is practicality;			
		let o be a random number between 1 and the number of entries in operations;
		let operation be entry o of operations;
		let right be the turn count;
		let left be a number;
		if operation is "x" and remainder after dividing answer by turn count is not 0:
			while operation is "x":
				now o is a random number between 1 and the number of entries in operations;
				now operation is entry o of operations;
		if operation is "x":
			now left is answer divided by turn count;
		else if operation is "/":
			now left is answer multiplied by turn count;
		else if operation is "+":
			now left is answer minus turn count;
		else if operation is "-":
			now left is answer plus turn count; 
		say "[left] [operation] [right] = _____[line break]".

Rule for printing a parser error when the latest parser error is not a verb I recognise error and the player's command matches the text "math":
	say "To answer a math question, type [bold type]math, 42[roman type] (replace 42 with whatever number you think is the answer to one of the questions).".

Instead of answering the math that something:
	if the player's command matches the regular expression "\d+":
		let num be the substituted form of "[text matching regular expression]";
		if num exactly matches the text "[boldness]":
			say "A bold (and correct) move!";
		if num exactly matches the text "[weirdness]":
			say "Weird, but accurate!";
		if num exactly matches the text "[rudeness]":
			say "Yes, but there's no need to be so rude about it!";
		if num exactly matches the text "[politeness]":
			say "Solving it effortlessly, how polite!";
		if num exactly matches the text "[practicality]":
			say "Good work, you practical problem-solver!";
		else:
			say "Incorrect, that number is not the solution to any of the equations.";
	else:
		say "To answer a math question, type [bold type]math, 42[roman type] (replace 42 with whatever number you think is the answer to one of the questions).";

Attacking the math is practical behavior.
Instead of attacking the math:
	say "You wrestle with the equations. [The scientist] gives a sympathetic nod. Math is hard!".

Instead of climbing the math:
	say "You've lost count of how many times you've had a terrible idea, but you know this is one of them. Math is decidedly un-climbable. [The scientist]  [their] head."

Cutting the math is rude behavior. Cutting the math is weird behavior.
Instead of cutting the math when the knife is carried:
	say "You slice up the paper a bit. How dare math exist in this world! The nerve of some disciplines! [The scientist] apparently does not agree with this assessment, as you can tell from [their] wincing.".

Report dropping the math:
	say "You let go of the math worksheet and feel a great weight fall off of your shoulders. [The scientist] gives an understanding nod.".

Instead of eating the math:
	say "You bite into the math worksheet. Paper is not a good taste[if the math is wet], especially not wet paper[end if]!".

Inserting the math into the scientist is weird behavior.
Instead of inserting the math into the scientist:
	say "[The scientist] is [italic type]made[roman type] of math! I'm not sure you could fit more math in if you tried! [They] chuckles at the attempt, though."

Inserting the math into the coat is polite behavior.
Instead of inserting the math into the coat:
	say "You try to tuck the math worksheet into [the scientist]'s coat, thinking [they] might have need of it, but [they] doesn't seem interested and hands the worksheet back to you, shaking [their] head.".

Instead of inserting the math into the clipboard:
	try putting the math on the clipboard.

Putting the math on the clipboard is rude behavior.
Instead of putting the math on the clipboard:
	say "You try to shove the math worksheet onto [the scientist]'s clipboard, but [they] shoves it right back to you with a disapproving look.".

Instead of kissing the math:
	say "All your romantic attempts add up to nothing, for mathematics is a cruel, uncaring mistress.".

Knocking the math is weird behavior.
Instead of knocking the math:
	let n be a random number between 2 and 5;
	let r be n times turn count;
	say "You knock [n] times and get [r] knocks in response. [The scientist] shrugs.".

Stabbing the math is rude behavior.
Instead of stabbing the math when the knife is carried:
	say "You dramatically plunge the knife into the math worksheet! Take [italic type]that[roman type], arithmetic! [The scientist] arches a brow.";
	now the math is chopped;
	if the math is on the table:
		say "It's not a particularly thick paper, so the knife stabs clean through to the table.";
		try stabbing the table.

Throwing the math at something is weird behavior.
Instead of throwing the math at something:
	say "You throw the math worksheet at [the second noun], but it floats like paper and doesn't end up going far at all. [The scientist] makes an exaggerated motion as if that's supposed to help somehow.".

Wearing the math is weird behavior.
Instead of wearing the math:
	say "You wrap the sheet of math around you like a cloak. The count is [italic type]in[roman type]!".

test scientist with "x scientist / attack scientist / burn coat / burn scientist / burn clipboard / burn pencil / buy scientist / climb scientist / climb pencil / climb coat / climb clipboard / cut scientist / cut coat / drink scientist / drink pencil / eat pencil / eat scientist / eat clipboard / kiss scientist / ask scientist about kiss / kiss scientist / kiss pencil / kiss clipboard / kiss coat / knock clipboard / knock pencil / knock scientist / knock clipboard / listen to scientist / listen to clipboard / listen to pencil / ask scientist about score / ask scientist about personality / ask scientist about test / listen to scientist / pull scientist / push scientist / wave / x coat / x pencil / x clipboard / wave knife / ask scientist about results / yes".

[Your bold actions were climbing the scientist, climbing the pencil, climbing the white lab coat (pristine), and climbing the clipboard.
Your weird actions were eating the pencil, eating the scientist, eating the clipboard, kissing the pencil, kissing the clipboard, and kissing the white lab coat (pristine).
Your rude actions were attacking the scientist, burning the white lab coat (pristine), burning the scientist, burning the clipboard, burning the pencil, climbing the scientist, cutting the scientist, cutting the white lab coat (pristine), eating the scientist, pulling the scientist, and pushing the scientist.
Your polite actions were asking the scientist about "kiss", listening to the scientist, and waving hands.
Your practical actions were examining the scientist, listening to the scientist, examining the white lab coat (pristine), examining the pencil, and examining the clipboard.]

test me with "attack me / pull me / push me / burn me / climb me / eat me / ask scientist about results / yes".

[Your bold actions were climbing yourself.
Your weird actions were eating yourself.
Your rude actions were attacking yourself, pulling yourself, pushing yourself, burning yourself, and eating yourself.
Your polite actions were .
Your practical actions were ]

test table with "look / x table / attack table / cut table / burn table / look / x table / climb table / push scientist / eat table / knock table / throw table at scientist / wear table / kiss table / throw table at coat / throw table at clipboard / throw table at pencil / throw table at salad / throw table at knife / throw table at math / throw table at lake / throw table at me / throw table at table / ask scientist about results / yes".

[Your bold actions were .
Your weird actions were attacking the scratched table, climbing the scratched table, eating the scratched table, wearing the scratched table, and kissing the scratched table.
Your rude actions were cutting the scratched table, burning the scratched table, and pushing the scientist.
Your polite actions were .
Your practical actions were examining the scratched table and knocking the scratched table]

test salad with "look / burn salad / climb salad / x salad / hit salad / hit salad / eat pieces / hit salad / x pieces / wear pieces / wait / wear salad / eat hat / wear pieces / x hat / wear pieces / x me / wear pieces / eat hat / wear salad / hit salad / look / x pieces / put salad on table / eat pieces / push salad / look / put salad on table / look / pull salad / look / take salad / drop salad / wear pieces / eat hat / put salad on table / pull salad / look / eat pieces / put salad on table / push salad / look / throw salad at scientist / x scientist / x coat / throw salad at pencil / throw salad at clipboard / throw salad at coat / throw salad at table / throw salad at pieces / wear pieces / throw salad at hat / wear pieces / eat hat / throw salad at table / throw salad at knife / take pieces / put pieces in salad / i / x salad / cut salad / kiss salad / throw salad at me / throw salad at salad / eat salad / ask scientist about results / yes".

[Your bold actions were climbing the tainted salad, eating the few salad pieces, and eating the salad hat.
Your weird actions were eating the few salad pieces, wearing the few salad pieces, wearing the tainted salad, eating the salad hat, and kissing the tainted salad.
Your rude actions were burning the tainted salad, attacking the tainted salad, pushing the tainted salad, pulling the tainted salad, dropping the tainted salad, throwing the tainted salad at the scientist, throwing the tainted salad at the pencil, throwing the tainted salad at the clipboard, throwing the tainted salad at the white lab coat (tomato-stained ), throwing the tainted salad at the table, throwing the tainted salad at the few salad pieces, throwing the tainted salad at the salad hat, throwing the tainted salad at the knife, inserting the few salad pieces into the tainted salad, throwing the tainted salad at yourself, and throwing the tainted salad at the tainted salad.
Your polite actions were taking the few salad pieces, putting the tainted salad on the table, cutting the tainted salad, and kissing the tainted salad.
Your practical actions were examining the tainted salad, examining the few salad pieces, examining the salad hat, examining yourself, eating the salad hat, eating the few salad pieces, examining the scientist, examining the white lab coat (tomato-stained ), cutting the tainted salad, and eating the tainted salad]

test goodsalad with "x salad / hit salad / eat salad / eat salad pieces / ask scientist about results / yes".

[Your bold actions were eating the few salad pieces.
Your weird actions were eating the few salad pieces.
Your rude actions were attacking the salad.
Your polite actions were taking the few salad pieces.
Your practical actions were examining the salad and eating the salad]

test taintedsalad with "hit salad / put pieces in salad / x salad / eat salad / ask scientist about results / yes".

[Your bold actions were .
Your weird actions were .
Your rude actions were attacking the tainted salad and inserting the few salad pieces into the tainted salad.
Your polite actions were .
Your practical actions were examining the tainted salad and eating the tainted salad]

test unviewedsalad with "eat salad / ask scientist about results / yes".

[Your bold actions were eating the salad.
Your weird actions were .
Your rude actions were .
Your polite actions were .
Your practical actions were ]

test knife with "x knife / climb knife / take knife / cut coat / x scientist / x coat / take knife / cut coat / take knife / cut table / x table / cut salad / look / x salad / eat knife / put knife on salad / push knife / look / put knife on table / pull knife / throw knife at scientist / look / throw knife at coat / look / throw knife at table / x table / take knife / x table / throw knife at salad / hit salad / throw knife at pieces / wear pieces / x me / wear pieces / throw knife at hat / x me / wear knife / look / x me / drop knife / stab coat / take knife / stab salad / look / stab pieces / cut pencil / x pencil / stab pencil / x pencil / stab pencil / give pencil to scientist / x scientist / stab clipboard / cut clipboard / eat salad / kiss knife / x me / throw knife at me / throw knife at pencil / throw knife at clipboard / throw knife at knife / ask scientist about results / yes".

[Your bold actions were climbing the knife, pushing the knife, pulling the knife, throwing the knife at the scientist, throwing the knife at the white lab coat (with a single slash mark), throwing the knife at the scratched table, throwing the knife at the chopped salad, throwing the knife at the few salad pieces, throwing the knife at the salad hat, wearing the knife, stabbing the scientist, kissing the knife, throwing the knife at yourself, throwing the knife at the pencil (replacement), throwing the knife at the clipboard, and throwing the knife at the knife.
Your weird actions were eating the knife, putting the knife on the chopped salad, throwing the knife at the chopped salad, throwing the knife at the few salad pieces, wearing the few salad pieces, wearing the knife, and kissing the knife.
Your rude actions were cutting the white lab coat (with a single slash mark), cutting the scratched table, throwing the knife at the scientist, throwing the knife at the scratched table, attacking the chopped salad, stabbing the scientist, cutting the pencil (replacement), stabbing the pencil (replacement), and cutting the clipboard.
Your polite actions were cutting the chopped salad, putting the knife on the scratched table, taking the few salad pieces, and giving the pencil (replacement) to the scientist.
Your practical actions were examining the knife, examining the scientist, examining the white lab coat (with a single slash mark), examining the scratched table, cutting the chopped salad, examining the chopped salad, putting the knife on the scratched table, examining yourself, examining the pencil (replacement), and eating the chopped salad]

test secondknife with "x knife / x scientist / take knife / stab scientist / x scientist / x coat / x table / take knife / stab table / x table / take knife / cut lake / look / drink lake / eat lake / kiss lake / knock lake / listen to lake / pull lake / push lake / ask scientist about results / yes"

[Your bold actions were stabbing the scientist, stabbing the table, pulling the lake, and pushing the lake.
Your weird actions were cutting the lake, drinking the lake, eating the lake, kissing the lake, and knocking the lake.
Your rude actions were stabbing the scientist and stabbing the table.
Your polite actions were listening to the lake.
Your practical actions were examining the knife, examining the scientist, examining the white lab coat (with a single slash mark), examining the table, and listening to the lake]

test lake with "look / eat lake / drink lake / x me / x lake / enter lake / x me / x scientist / leave lake / attack lake / x scientist / climb lake / exit lake / climb lake / x scientist / x coat / x clipboard / x pencil / get out of lake / drink me / x me / drink scientist / x me / drink me / x me / swim lake / attack lake / take knife / get out lake / cut lake / x knife / drink lake / eat lake / kiss lake / knock lake / listen to lake / pull lake / push lake / take knife / put knife on lake / take knife / stab lake / take knife / throw knife at lake / wear lake / wear lake / exit / wear salad / x salad hat / x pieces / eat pieces / drink me / throw salad at lake / x salad / put salad on table / hit salad / x pieces / eat pieces / drink salad / x salad / hit salad / wear pieces / x hat / x pieces / throw salad at lake / eat salad / throw lake at lake / ask scientist about results / yes".

[Your bold actions were climbing the lake, pulling the lake, pushing the lake, and throwing the knife at the lake.
Your weird actions were eating the lake, drinking the lake, attacking the lake, climbing the lake, drinking yourself, drinking the scientist, cutting the lake, kissing the lake, knocking the lake, wearing the lake, wearing the soggy salad, eating the few salad pieces, drinking the soggy salad, and wearing the few salad pieces.
Your rude actions were inserting the knife into the lake, throwing the soggy salad at the lake, inserting the soggy salad into the lake, attacking the soggy salad, throwing the lake at the lake, and inserting the lake into the lake.
Your polite actions were listening to the lake, taking the few salad pieces, and putting the soggy salad on the table.
Your practical actions were examining yourself, examining the lake, examining the scientist, examining the white lab coat (pristine), examining the clipboard, examining the pencil, examining the knife, listening to the lake, examining the salad hat, examining the few salad pieces, eating the few salad pieces, examining the soggy salad, and eating the soggy salad]

test wetness with "attack pencil / take pencil / put pencil in lake / take pencil / drink pencil / put salad in lake / take salad / hit salad / wear pieces / drink pieces / put pieces in lake / take pieces / drink pieces / drink salad / put knife in lake / take knife / drink knife / put math in lake / take math / drink math / throw lake at scientist / drink scientist / throw lake at clipboard / drink clipboard / throw lake at pencil / drink pencil / throw lake at coat / drink coat / throw lake at table / drink table / throw lake at salad / hit salad / throw lake at pieces / wear pieces / throw lake at hat / throw lake at knife / throw lake at math / ask scientist about results / yes".

[Your bold actions were .
Your weird actions were wearing the few salad pieces, drinking the soggy salad, drinking the knife, and drinking the math worksheet.
Your rude actions were attacking the pencil (replacement), inserting the pencil (replacement) into the lake, inserting the soggy salad into the lake, attacking the soggy salad, inserting the knife into the lake, inserting the math worksheet into the lake, throwing the lake at the scientist, throwing the lake at the clipboard, throwing the lake at the pencil (replacement), throwing the lake at the white lab coat (pristine), throwing the lake at the table, throwing the lake at the soggy salad, throwing the lake at the few salad pieces, throwing the lake at the salad hat, throwing the lake at the knife, and throwing the lake at the math worksheet.
Your polite actions were taking the few salad pieces.
Your practical actions were ]

test math with "math, 0 / math, 4 / math, hi / do math / attack math / climb math / take knife / cut math / drop math / x math / x math / eat math / put math in scientist / put paper in coat / put math on clipboard / kiss math / knock on math / throw math at lake / eat math / stab math / x math / x table / wear math / throw math at scientist / throw math at coat / throw math at clipboard / throw math at pencil / throw math at table / throw math at salad / throw math at lake / throw math at me / ask scientist about results / yes".

[Your bold actions were climbing the math worksheet.
Your weird actions were cutting the math worksheet, eating the math worksheet, inserting the math worksheet into the scientist, kissing the math worksheet, knocking the math worksheet, throwing the math worksheet at the lake, wearing the math worksheet, throwing the math worksheet at the scientist, throwing the math worksheet at the white lab coat (pristine), throwing the math worksheet at the clipboard, throwing the math worksheet at the pencil, throwing the math worksheet at the table, throwing the math worksheet at the salad, and throwing the math worksheet at yourself.
Your rude actions were cutting the math worksheet, putting the math worksheet on the clipboard, inserting the math worksheet into the lake, and stabbing the math worksheet.
Your polite actions were inserting the math worksheet into the white lab coat (pristine).
Your practical actions were attacking the math worksheet, examining the math worksheet, and examining the table]

test math2 with "take knife / stab math / x math / x table / ask scientist about results / yes".

[Your bold actions were stabbing the table.
Your weird actions were .
Your rude actions were stabbing the math worksheet and stabbing the table.
Your polite actions were .
Your practical actions were examining the math worksheet and examining the table]

[
bold: 31
{'pushing the knife', 'pulling the lake', 'climbing the knife', 'stabbing the table', 'throwing the knife at the scientist', 'throwing the knife at the pencil', 'eating the few salad pieces', 'pulling the knife', 'throwing the knife at yourself', 'climbing the scientist', 'throwing the knife at the lake', 'eating the salad hat', 'climbing the salad', 'climbing the math worksheet', 'throwing the knife at the clipboard', 'throwing the knife at the knife', 'climbing the lake', 'climbing the clipboard', 'climbing the pencil', 'throwing the knife at the salad hat', 'eating the salad', 'pushing the lake', 'throwing the knife at the few salad pieces', 'throwing the knife at the table', 'wearing the knife', 'climbing the white lab coat', 'climbing yourself', 'stabbing the scientist', 'throwing the knife at the salad', 'throwing the knife at the white lab coat', 'kissing the knife'}
weird: 50
{'kissing the pencil', 'knocking the lake', 'eating the scientist', 'eating the clipboard', 'wearing the salad', 'eating yourself', 'wearing the table', 'cutting the math worksheet', 'eating the table', 'kissing the math worksheet', 'throwing the math worksheet at the lake', 'throwing the math worksheet at the pencil', 'drinking the math worksheet', 'drinking the knife', 'eating the knife', 'wearing the lake', 'throwing the math worksheet at the clipboard', 'throwing the math worksheet at the salad', 'eating the few salad pieces', 'cutting the lake', 'attacking the lake', 'wearing the math worksheet', 'throwing the math worksheet at yourself', 'attacking the table', 'throwing the knife at the chopped salad', 'wearing the few salad pieces', 'drinking the lake', 'kissing the table', 'eating the salad hat', 'kissing the clipboard', 'throwing the math worksheet at the white lab coat', 'inserting the math worksheet into the scientist', 'throwing the math worksheet at the table', 'kissing the lake', 'eating the pencil', 'eating the math worksheet', 'climbing the lake', 'putting the knife on the salad', 'kissing the white lab coat', 'climbing the table', 'throwing the knife at the few salad pieces', 'drinking the scientist', 'wearing the knife', 'drinking yourself', 'eating the lake', 'kissing the salad', 'knocking the math worksheet', 'drinking the salad', 'kissing the knife', 'throwing the math worksheet at the scientist'}
rude: 59
{'throwing the salad at the salad', 'pulling the scientist', 'throwing the salad at the white lab coat', 'burning yourself', 'throwing the lake at the salad hat', 'eating the scientist', 'throwing the salad at the lake', 'pulling the salad', 'attacking the salad inserting the few salad pieces into the salad', 'eating yourself', 'stabbing the table', 'throwing the knife at the scientist', 'throwing the lake at the clipboard', 'throwing the lake at the white lab coat', 'cutting the math worksheet', 'putting the math worksheet on the clipboard', 'pushing the scientist', 'cutting the pencil', 'cutting the table', 'attacking the salad', 'throwing the salad at the scientist', 'cutting the scientist', 'burning the table', 'throwing the salad at the knife', 'inserting the math worksheet into the lake', 'throwing the salad at the few salad pieces', 'throwing the lake at the lake', 'climbing the scientist', 'inserting the pencil into the lake', 'throwing the lake at the math worksheet', 'attacking yourself', 'inserting the lake into the lake', 'burning the scientist', 'throwing the lake at the few salad pieces', 'throwing the lake at the pencil', 'throwing the lake at the table', 'throwing the lake at the knife', 'stabbing the math worksheet', 'dropping the salad', 'stabbing the pencil', 'attacking the pencil', 'throwing the lake at the salad', 'throwing the salad at the salad hat', 'throwing the salad at yourself', 'inserting the salad into the lake', 'attacking the scientist', 'throwing the knife at the table', 'throwing the lake at the scientist', 'cutting the white lab coat', 'pushing the salad', 'throwing the salad at the table', 'throwing the salad at the clipboard', 'pulling yourself', 'cutting the clipboard', 'stabbing the scientist', 'inserting the knife into the lake', 'pushing yourself', 'throwing the salad at the pencil', 'inserting the few salad pieces into the salad'}
polite: 9
{'listening to the lake', 'putting the salad on the table', 'cutting the salad', 'putting the knife on the table', 'giving the pencil to the scientist', 'inserting the math worksheet into the white lab coat', 'kissing the salad', 'taking the few salad pieces', 'asking the scientist about '}
practical: 22
{'examining yourself', 'examining the white lab coat', 'examining the salad', 'examining the scientist', 'eating the few salad pieces', 'listening to the lake', 'listening to the scientist', 'examining the table knocking the table', 'examining the math worksheet', 'eating the salad hat', 'examining the clipboard', 'examining the salad hat', 'examining the few salad pieces', 'examining the pencil', 'eating the salad', 'examining the scratched table', 'attacking the math worksheet', 'examining the lake', 'cutting the salad', 'putting the knife on the table', 'examining the table', 'examining the knife'}
]
